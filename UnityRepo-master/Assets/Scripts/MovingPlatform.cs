﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    
    float distanceStart;
    float distanceEnd;
    public float distance;

    static float distanceMid = 0f;

    public float speed;

    public GameObject craneLights;

	// Use this for initialization
	void Start ()
    {
        distanceEnd = transform.position.x + distance;
        distanceStart = transform.position.x;
	}
	
	// Update is called once per frame
	void Update ()
    {

    }
    public void Move()
    {
        transform.position = new Vector2(Mathf.Lerp(distanceStart, distanceEnd, distanceMid), transform.position.y);

        distanceMid += 0.5f * speed * Time.deltaTime;

        if (distanceMid > 1)
        {
            float temp = distanceEnd;
            distanceEnd = distanceStart;
            distanceStart = temp;
            distanceMid = 0f;
        }
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Move();
            craneLights.SetActive(true);
        }
    }

    public void OnCollisionExit2D(Collision2D collision)

    {
        if(collision.gameObject.CompareTag("Player"))
        {
            craneLights.SetActive(false);
        }
    }



}
