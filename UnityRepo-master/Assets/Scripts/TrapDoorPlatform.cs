﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDoorPlatform : MonoBehaviour {

    public float DelayTime;
    Rigidbody2D rb;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    IEnumerator PlatformTrapDoor()
    {
        yield return new WaitForSeconds(DelayTime);
        rb.constraints = RigidbodyConstraints2D.None;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(PlatformTrapDoor());
    }
}
