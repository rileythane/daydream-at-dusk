﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneMovement : MonoBehaviour
{

    //creates some private vectors for the positions of the lift
    private Vector3 posA;
    private Vector3 posB;
    private Vector3 nexPos;
    public float WaitTime = 5f;
    bool CanMove = true;
    public GameObject car;
    [SerializeField]
    //creates the speed of the lift
    private float speed;
    [SerializeField]
    //transforms the positon
    private Transform childTransform;
    [SerializeField]
    //creates the second position of the lift
    private Transform TransformB;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(canMove());
        //transforms the position of the lift between posA and posB
        posA = childTransform.localPosition;
        posB = TransformB.localPosition;
        nexPos = posB;
    }

    // Update is called once per frame
    void Update()
    {
        //tells to run move function upon update
        if (CanMove == true)
        {
            Move();
        }

        if(transform.position == posB)
        {
            transform.position = posA;
        }

    }

    private void Move()
    {
        //transforms between positions at a certain speed
        //if the distance is less than or equal to 0.1 it will change the destination of the lift
        //so it goes up and down
        childTransform.localPosition = Vector3.MoveTowards(childTransform.localPosition, nexPos, speed * Time.deltaTime);
        if (Vector3.Distance(childTransform.localPosition, nexPos) <= 0.1)
        {
         //   Destroy(car);
        }
    }


    public IEnumerator canMove()
    {

        CanMove = false;
        yield return new WaitForSeconds(WaitTime);
        CanMove = true;

    }
}