﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spark : MonoBehaviour
{


    public float smoothdamp;
    public Vector3 offset;
    private Vector3 velocity;
    public List<Transform> player;

    public Transform playerPos;

    public PlayerMovement PM;

    // Use this for initialization
    void Start()
    {

    }

    void FixedUpdate()
    {

        Vector3 centorPoint = GetCentorPoint();

        Vector3 newPosition = centorPoint + offset;

        // transform.position = playerPos.transform.position + offset;
        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothdamp);
    }


    Vector3 GetCentorPoint()
    {
        if (player.Count == 1)
            return player[0].position;

        Bounds bounds = new Bounds(player[0].position, Vector3.zero);
        for (int i = 0; i < player.Count; i++)
        {
            bounds.Encapsulate(player[i].position);
        }
        return bounds.center;
    }
}
