﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensitivityChanger : MonoBehaviour
{

    public float currTrackTime;
    public AudioSource audSrc;
    public AudioProcessor audProc;
    public float startSens, firstPianoSens, bellsSens, secondPianoSens,
                 scaleAndBellSens, churchPianoSens, churchPianoWithBass, chordChangeSens,
                 drumsAddedSens, drumAndBellSens, glideSynthSens, glideSynthOutroSens, drumOutroSens,
                 pianoOutroSens;

    // Use this for initialization
    void Start()
    {
        audProc = GetComponent<AudioProcessor>();

        audProc.gThresh = startSens;
    }

    // Update is called once per frame
    void Update()
    {

        currTrackTime = audSrc.time;


        if (currTrackTime > 15f && currTrackTime < 16f)
        {
            audProc.gThresh = firstPianoSens;
        }

        if (currTrackTime > 30f && currTrackTime < 31f)
        {
            audProc.gThresh = bellsSens;
        }

        if (currTrackTime > 45f && currTrackTime < 46f)
        {
            audProc.gThresh = secondPianoSens;
        }

        if (currTrackTime > 53f && currTrackTime < 54f)
        {
            audProc.gThresh = scaleAndBellSens;
        }

        if (currTrackTime > 68f && currTrackTime < 69f)
        {
            audProc.gThresh = churchPianoSens;
        }

        if (currTrackTime > 76f && currTrackTime < 77f)
        {
            audProc.gThresh = churchPianoWithBass;
        }

        if (currTrackTime > 91f && currTrackTime < 92f)
        {
            audProc.gThresh = chordChangeSens;
        }

        if (currTrackTime > 114f && currTrackTime < 115f)
        {
            audProc.gThresh = drumsAddedSens;
        }

        if (currTrackTime > 129f && currTrackTime < 130)
        {
            audProc.gThresh = drumAndBellSens;
        }

        if (currTrackTime > 145f && currTrackTime < 146)
        {
            audProc.gThresh = glideSynthSens;
        }

        if (currTrackTime > 175f && currTrackTime < 176)
        {
            audProc.gThresh = glideSynthOutroSens;
        }

        if (currTrackTime > 191f && currTrackTime < 192f)
        {
            audProc.gThresh = drumOutroSens;
        }

        if (currTrackTime > 203f && currTrackTime < 204f)
        {
            audProc.gThresh = pianoOutroSens;
        }
    }
}
