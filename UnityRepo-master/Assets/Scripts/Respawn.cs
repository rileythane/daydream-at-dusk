﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Respawn : MonoBehaviour
{

    public GameObject player;
    public GameObject spawnPoint;
    public Vector3 deathDir;
    public Vector3 respawnOffset;
    Rigidbody2D rb;
    public bool cameraActive;
    public Checkpoints cp;
    public AudioSource audioSource;
    public PlayerMovement pl;
    public SpriteRenderer sprt;
    public float currentAudioCheckPoint;

    public GameObject LevelRestart;
    public bool LevelEnd = false;

    public Image White;
    public Animator anim;
    public float TransitionTime, upForce;
    public CameraScript cs;
    public GameObject clouds, moon;
    public GameObject spark, roomLights, sparkParticle, clouds2;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprt = GetComponent<SpriteRenderer>();
        audioSource.GetComponent<AudioSource>();
        respawnOffset = new Vector3(-5, 10, 0);
        TransitionTime = 10f;

        audioSource.time = 0f;
    }

    IEnumerator FallWait()
    {
        yield return new WaitForSeconds(0.75f);
        rb.simulated = true;

        StartCoroutine(AudioFadeOut.FadeIn(audioSource, 3f));
    }

    // Update is called once per frame
    void Update()
    {
        currentAudioCheckPoint = audioSource.time;
        //Debug.Log(currentAudioCheckPoint);

        if(LevelEnd == true)
        {
            StartCoroutine(Fading());
        }



    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("EndLevel"))
        {
            LevelEnd = true;
            clouds.SetActive(true);
            moon.SetActive(true);
            spark.SetActive(false);
            sparkParticle.SetActive(false);
            roomLights.SetActive(false);
            rb.mass = 12;
            Up();
        }

        if (other.gameObject.CompareTag("Kill"))
        {
            sprt.enabled = false;
            
        }

        if (other.gameObject.CompareTag("Clouds"))
        {
            clouds2.SetActive(true);
        }
    }

    public void OnCollisionEnter2D(Collision2D coll2D)
    {
        if (coll2D.gameObject.CompareTag("Bed"))
            player.transform.Rotate(0, 0, 90);
    }

    void Up()
    {
        rb.gravityScale = 0.05f;
    }

    IEnumerator Fading()
    {
        print("isFading");
       // anim.SetBool("Fade", true);
        yield return new WaitForSeconds(TransitionTime);
        anim.SetBool("Fade", true);
      

        // yield return new WaitUntil(() => White.color.a == 1);
    }

}
