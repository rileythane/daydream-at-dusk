﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransistion : MonoBehaviour {

    public CameraScript CS;
    public bool inBedroom;

	// Use this for initialization
	void Start ()
    {
        inBedroom = true;

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (inBedroom == true)
        {
            CS.offset = new Vector3(30f, 0f, -100f);
            CS.maxZoom = 30f;
            //CS.smoothdamp = 100f;
        }

        if(inBedroom == false)
        {
            CS.offset = new Vector3(12.5f,35f,-100f);
            CS.maxZoom = 50f;
            CS.smoothdamp = 0.25f;
        }
    }


}
