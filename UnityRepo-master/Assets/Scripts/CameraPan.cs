﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPan : MonoBehaviour
{

    public Animator anim;
    public CameraScript cs;
    public AnimationClip pan;
    public float pauseLength;
    public float counter;
    public AudioSource source;

    // Use this for initialization
    void Start()
    {
        cs.enabled = false;
        anim.enabled = false;
        pauseLength = pan.length + 0.15f;
        source.enabled = false;

    }

    IEnumerator PanPause()
    {


        yield return new WaitForSeconds(1f);
        anim.enabled = true;
        source.enabled = true;
        source.Play();

        StartCoroutine(CameraOn());
    }

    IEnumerator CameraOn()
    {
        yield return new WaitForSeconds(pauseLength);
        anim.enabled = false;
        cs.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))   
        {
            counter++;
        }

        if(counter == 1f)
        {
            StartCoroutine(PanPause());
        }
    }
}
