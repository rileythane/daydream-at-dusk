﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BouncePad : MonoBehaviour
{

    public float bounceStrength, rightStrength;
    public SpriteRenderer sprt;
    public Sprite TruckDip;
    public Sprite Truck;
    public GameObject truckLights;
    public CameraScript cs;


    // Use this for initialization
    void Start()
    {
        sprt = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator ColorWait()
    {
        yield return new WaitForSeconds(0.1f);
        sprt.color = Color.white;
        sprt.sprite = Truck;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            sprt.color = Color.blue;
            sprt.sprite = TruckDip;
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bounceStrength);
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * rightStrength);
            StartCoroutine(LightWait());
        }
    }

    public void OnCollisionExit2D(Collision2D coll)
    {
        StartCoroutine(ColorWait());
    }

    public IEnumerator LightWait()
    {
        truckLights.SetActive(true);

        yield return new WaitForSeconds(2f);
        {
            truckLights.SetActive(false);
        }
        
    }

}
