﻿/*
 * Copyright (c) 2015 Allan Pichardo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using UnityEngine;
using System;
using System.Collections.Generic;

public class Example : MonoBehaviour
{

    public GameObject[] odd, even, random;
    public SpriteRenderer evenRend, oddRend, randRend;


    public int counter;
    public int rnd;

    void Start()
    {
        //Select the instance of AudioProcessor and pass a reference
        //to this object
        AudioProcessor processor = FindObjectOfType<AudioProcessor>();
        processor.onBeat.AddListener(onOnbeatDetected);
        processor.onSpectrum.AddListener(onSpectrum);
    }


    //this event will be called every time a beat is detected.
    //Change the threshold parameter in the inspector
    //to adjust the sensitivity
    void onOnbeatDetected()
    {
        counter++;
        Debug.Log(counter);

        RandomFlash();
        OddFlash();
        EvenFlash();
    }


    void OddFlash()
    {
        if (counter % 2 == 0)
        {
            odd = GameObject.FindGameObjectsWithTag("OddSprites");
            even = GameObject.FindGameObjectsWithTag("EvenSprites");

            foreach (GameObject odds in odd)
            {
                oddRend = odds.GetComponent<SpriteRenderer>();
                oddRend.enabled = true;
                
            }

            foreach (GameObject evens in even)
            {
                evenRend = evens.GetComponent<SpriteRenderer>();
                evenRend.enabled = false;
            }

        }
    }

    void EvenFlash()
    {
        if (counter % 2 == 1)
        {
            odd = GameObject.FindGameObjectsWithTag("OddSprites");
            even = GameObject.FindGameObjectsWithTag("EvenSprites");

            foreach (GameObject odds in odd)
            {
                oddRend = odds.GetComponent<SpriteRenderer>();
                oddRend.enabled = false;

            }

            foreach (GameObject evens in even)
            {
                evenRend = evens.GetComponent<SpriteRenderer>();
                evenRend.enabled = true;
            }
        }
    }

    void RandomFlash()
    {
        rnd = UnityEngine.Random.Range(0, 10);
        random = GameObject.FindGameObjectsWithTag("RandomSprites");

        if (rnd % 2 == 0)
        {
            foreach (GameObject rands in random)
            {
                randRend = rands.GetComponent<SpriteRenderer>();
                randRend.enabled = true;
            }
        }

        if (rnd % 2 == 1)
        {
            foreach (GameObject rands in random)
            {
                randRend = rands.GetComponent<SpriteRenderer>();
                randRend.enabled = false;
            }
        }
    }



    //This event will be called every frame while music is playing
    void onSpectrum(float[] spectrum)
    {
        //The spectrum is logarithmically averaged
        //to 12 bands

        for (int i = 0; i < spectrum.Length; ++i)
        {
            Vector3 start = new Vector3(i, 0, 0);
            Vector3 end = new Vector3(i, spectrum[i], 0);
            Debug.DrawLine(start, end);
        }
    }
}
