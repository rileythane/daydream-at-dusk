﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bed : MonoBehaviour
{

    public float bounceStrength;
    public SpriteRenderer sprt;
    public Sprite bed;
    public Sprite bedBounce;
    public GameObject player;
    public Rigidbody2D rb;

    // Use this for initialization
    void Start()
    {
        sprt = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator BounceTime()
    {
        yield return new WaitForSeconds(0.1f);
        sprt.sprite = bed;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            sprt.sprite = bedBounce;
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * bounceStrength);
            print("bounce bed");
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        StartCoroutine(BounceTime());
    }
}
