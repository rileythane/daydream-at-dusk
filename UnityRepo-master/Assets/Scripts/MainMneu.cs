﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMneu : MonoBehaviour {

    public bool GamePaused = true;

    public GameObject MenuUI;
    public GameObject CreditsUI;

	// Use this for initialization
	void Start ()
    {
        Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            print("Is Paused");
            GamePaused = true;
            Time.timeScale = 0;
            MenuUI.SetActive(true);
            Cursor.visible = true;
        }
	}

    public void Resume()
    {
        GamePaused = false;
        Time.timeScale = 1;
        MenuUI.SetActive(false);
        Cursor.visible = false;
    }

    public void Credits()
    {
        Time.timeScale = 0;
        CreditsUI.SetActive(true);
        MenuUI.SetActive(false);
    }

    public void ReturnToMenu()
    {
        CreditsUI.SetActive(false);
        MenuUI.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }

}
