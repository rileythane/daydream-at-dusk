﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdBehaviour : MonoBehaviour
{
    public AnimationClip flyAway;
    public Rigidbody2D rb;
    public Vector2 up;
    public Transform birdSpawn;
    public ParticleSystem part;
    public float rndx, rndy;
    private float size;

    // Use this for initialization
    void Start()
    {
        rndx = Random.Range(10f, 25f);
        rndy = Random.Range(10f, 25f);
        size = Random.Range(2f, 4.5f);

        rb = GetComponent<Rigidbody2D>();
        up = new Vector2(rndx, rndy);

        gameObject.transform.localScale = new Vector3(size, size, 0);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        var newVelocity = rb.velocity;


        if (coll.gameObject.CompareTag("Player"))
        {

            gameObject.GetComponent<Animator>().enabled = true;
            newVelocity = up;
            rb.velocity = newVelocity;
            part.Play();

            StartCoroutine(Death());
        }
    }


    IEnumerator Death()
    {

        yield return new WaitForSeconds(10f);
        gameObject.SetActive(false);
        
    }
    // Update is called once per frame
    void Update()
    {

    }
}
