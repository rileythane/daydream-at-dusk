﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public float maxSpeed, maxJumpSpeed, counter, speed, t;
    Vector2 moving;
    public Vector3 lastPlatformTouched;
    public Checkpoints cp;
    public float downForce, jumpCount, rnd;
    public Respawn rsp;
    public LayerMask Ground;
    public Animator anim, camAnim;
    public AnimationClip jump, run;
    public SpriteRenderer windowRend, platformRend;
    public Color window1, window2, window3, window4, window5, window6, window7, window8,
                 windowStart, foreGround, foreGround2, midGround, midGround2, platForms, selectedWindow;

    public GameObject[] backGroundObj, midGroundObj, foreGroundObj, window;
    public GameObject player;
    public SpriteRenderer bg, mg, fg;
    public CameraScript cs;
    public Camera cam;
    public bool hasJumped;
    public bool inBedroom;

    public ParticleSystem Running;

    public bool StartMoving = false;


    // Use this for initialization

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        t = 0f;
        rb.mass = 2.2f;

        window = GameObject.FindGameObjectsWithTag("Window");
        foreach (GameObject wind in window)
        {
            windowRend = wind.GetComponent<SpriteRenderer>();
            windowRend.color = windowStart;
        }

        inBedroom = true;

    }

   // private float startJumpTime = 0;
    [SerializeField]
   // private float minJumpTime = 1.5f;

    void JumpStuff()
    {
        var newVelocity = rb.velocity;

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
           // startJumpTime = Time.time;
            hasJumped = true;

            WindowFlash();
            anim.Play(jump.name);
            StartCoroutine(JumpWait());
            newVelocity.y = maxJumpSpeed;
        }

      //  if (Input.GetKey(KeyCode.Space) == false && hasJumped && (Time.time - startJumpTime >= minJumpTime) && newVelocity.y > 0)
      //  {
       //     newVelocity.y = 5;
       //     hasJumped = false;
      //  }

        newVelocity.x = maxSpeed;

        rb.velocity = newVelocity;
    }

    void Update()
    {
        PushDown();
        JumpStuff();

        if (rsp.LevelEnd == true)
        {
            StartMoving = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartMoving = true;
            anim.Play(run.name);
        }

        if (StartMoving == false)
        {
            maxSpeed = 0;
            anim.Play(jump.name);
        }
        else
        {
            maxSpeed = 23.5f;
        }

        if (counter % 2 == 0)
        {
            midGroundObj = GameObject.FindGameObjectsWithTag("MidGround");
            foreGroundObj = GameObject.FindGameObjectsWithTag("ForeGround");

            foreach (GameObject fores in foreGroundObj)
            {
                fg = fores.GetComponent<SpriteRenderer>();
                fg.color = foreGround;
            }

            foreach (GameObject mids in midGroundObj)
            {
                mg = mids.GetComponent<SpriteRenderer>();
                mg.color = midGround;
            }
        }

        if (counter % 2 == 1)
        {
            midGroundObj = GameObject.FindGameObjectsWithTag("MidGround");
            foreGroundObj = GameObject.FindGameObjectsWithTag("ForeGround");

            foreach (GameObject fores in foreGroundObj)
            {
                fg = fores.GetComponent<SpriteRenderer>();
                fg.color = foreGround2;
            }

            foreach (GameObject mids in midGroundObj)
            {
                mg = mids.GetComponent<SpriteRenderer>();
                mg.color = midGround2;
            }

        }

        if (IsGrounded() == true)
        {
            Running.gameObject.SetActive(true);
        }

        if (IsGrounded() == false)
        {
            Running.gameObject.SetActive(false);
        }

        if (inBedroom == true)
        {
            cs.offset = new Vector3(30f, 0f, -100f);
            cs.maxZoom = 30f;

        }

        if (inBedroom == false)
        {
            cs.offset = new Vector3(36f, 25f, -100f);
            cs.maxZoom = 50f;

        }

    }

    public IEnumerator JumpWait()
    {
        yield return new WaitForSeconds(run.length - 0.35f);
        anim.Play(run.name);
    }

    public bool IsGrounded()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 3.0f;


        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, Ground);
        if (hit.collider != null)
        {
            return true;
        }

        return false;
    }

    void PushDown()
    {
        rb.AddForce(Vector2.down * downForce);
    }

    void WindowFlash()
    {
        counter++;
        window = GameObject.FindGameObjectsWithTag("Window");

        if (counter % 2 == 0)
        {
            foreach (GameObject windows in window)
            {
                {
                    windowRend = windows.GetComponent<SpriteRenderer>();
                    ColourSelect();
                    windowRend.color = selectedWindow;
                }
            }
        }

        if (counter % 2 == 1)
        {
            foreach (GameObject windows in window)
            {
                {
                    windowRend = windows.GetComponent<SpriteRenderer>();
                    ColourSelect();
                    windowRend.color = selectedWindow;
                }
            }
        }

    }

    void ColourSelect()
    {
        rnd = Random.Range(0, 8);

        if (rnd == 1)
        {
            selectedWindow = window1;
        }

        if (rnd == 2)
        {
            selectedWindow = window2;
        }

        if (rnd == 3)
        {
            selectedWindow = window3;
        }

        if (rnd == 4)
        {
            selectedWindow = window4;
        }

        if (rnd == 5)
        {
            selectedWindow = window5;
        }

        if (rnd == 6)
        {
            selectedWindow = window6;
        }

        if (rnd == 7)
        {
            selectedWindow = window7;
        }

        if (rnd == 8)
        {
            selectedWindow = window8;
        }

    }


    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Ground") || coll.gameObject.CompareTag("JumpPad") || coll.gameObject.CompareTag("FallingPlatform"))
        {
            //onGround = true;
            cp.currentSpawnPoint = coll.transform.position + rsp.respawnOffset;
            cs.smoothdamp = 0.25f;
            cam.orthographicSize = cs.currOrthSize;
            hasJumped = false;

        }

        if (coll.gameObject.CompareTag("Ground"))
        {
            platformRend = coll.gameObject.GetComponent<SpriteRenderer>();
            platformRend.color = platForms;
        }

        if (coll.gameObject.CompareTag("Truck"))
        {
            //cs.currOrthSize = cam.orthographicSize;
            cs.smoothdamp = 0.15f;
           // cam.orthographicSize = Mathf.Lerp(cs.currOrthSize, 58, 1);
        }

    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Ground"))
        {
            platformRend = coll.gameObject.GetComponent<SpriteRenderer>();
            StartCoroutine(PlatformColourChange());
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("OutWindow"))
        {
            inBedroom = false;
            rb.mass = 2.2f;
            Debug.Log("Hit");
        }

    }


    IEnumerator PlatformColourChange()
    {
        t = 0f;
        while (t <= 1f)
        {
            platformRend.color = Color.Lerp(platformRend.color, Color.white, t);
            t += Time.deltaTime * 2.5f;
            yield return null;
        }


    }

}
