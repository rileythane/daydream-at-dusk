﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clouds : MonoBehaviour {

    public Vector3 SecondPos;
    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Move();	
	}

    public void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, SecondPos, speed * Time.deltaTime);
    }
}
