﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPLatform : MonoBehaviour
{

    public float DelayTime = 0.35f;
    Rigidbody2D rb;
    public SpriteRenderer sprt;
    public Animation Anim;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        sprt = GetComponent<SpriteRenderer>();
    }

    IEnumerator PlatformFalls()
    {
        yield return new WaitForSeconds(DelayTime);
        rb.gravityScale = 1;
        rb.constraints = RigidbodyConstraints2D.None;
        print("yes");
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            sprt.color = Color.red;
            Anim.Play();
            StartCoroutine(PlatformFalls());
        }
    }


}
