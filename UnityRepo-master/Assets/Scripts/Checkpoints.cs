﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoints : MonoBehaviour
{
    public Vector3 currentSpawnPoint;
    public AudioSource audioSource;
    public float currentTime, respawnTrackTime;
    public GameObject player, startSpawn, playerSpawn;


    void Start()
    {
        currentSpawnPoint = playerSpawn.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime = audioSource.time;

        if(currentTime < 10)
        {
            respawnTrackTime = 0;
        }

        if(currentTime > 14f && currentTime < 15f)
        {
            respawnTrackTime = 14f;
        }

        if (currentTime > 30f && currentTime < 31f)
        {
            respawnTrackTime = 29f;
        }

        if (currentTime > 45f && currentTime < 46f)
        {
            respawnTrackTime = 44f;
        }

        if (currentTime > 53f && currentTime < 54f)
        {
            respawnTrackTime = 52f;
        }

        if (currentTime > 68f && currentTime < 69f)
        {
            respawnTrackTime = 67f;
        }

        if (currentTime > 76f && currentTime < 77f)
        {
            respawnTrackTime = 75f;
        }

        if (currentTime > 91f && currentTime < 92f)
        {
            respawnTrackTime = 90f;
        }

        if (currentTime > 114f && currentTime < 115f)
        {
            respawnTrackTime = 113f;
        }

        if (currentTime > 129f && currentTime < 130)
        {
            respawnTrackTime = 128f;
        }

        if (currentTime > 145f && currentTime < 146)
        {
            respawnTrackTime = 14f;
        }

        if (currentTime > 175f && currentTime < 176)
        {
            respawnTrackTime = 14f;
        }

        if (currentTime > 191f && currentTime < 192f)
        {
            respawnTrackTime = 14f;
        }

        if (currentTime > 203f && currentTime < 204f)
        {
            respawnTrackTime = 14f;
        }
    }
}
